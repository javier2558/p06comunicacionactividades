package com.example.gil25.comunicacionactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
   TextView Resultado;
   EditText ed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ed=(EditText)findViewById(R.id.NombreDado);
        Resultado=(TextView)findViewById((R.id.txtResultado));

    }
    public void CondicionesUsuarios(View view){
        Intent i=new Intent(this,Main2Activity.class);
        i.putExtra("Nombre",ed.getText().toString());
        startActivityForResult(i,  1234      );
    }
    @Override protected void onActivityResult (int requestCode,
                                               int resultCode, Intent data){
        if (requestCode==1234 && resultCode==RESULT_OK) {
            String resultado = data.getExtras().getString("resultado");
            Resultado.setText(resultado);
        }
    }

}
