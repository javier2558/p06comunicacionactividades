package com.example.gil25.comunicacionactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView saludo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        saludo=(TextView)findViewById(R.id.Nombre_Introducido2);

        Bundle extras = getIntent().getExtras();
        String nombre = extras.getString("Nombre");
        saludo.setText("Hola " +nombre+ " ¿Aceptas las condiciones?");









    }
    public void aceptar(View view){

        Intent intent = new Intent();
        intent.putExtra("resultado","Resultado:Aceptar");
        setResult(RESULT_OK, intent);
        finish();

    }
    public void rechazar(View view){

        Intent intent = new Intent();
        intent.putExtra("resultado","Resultado:Rechazar");
        setResult(RESULT_OK, intent);
        finish();

    }
}
